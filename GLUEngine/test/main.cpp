/**
 * export LD_LIBRARY_PATH where libengineGlut.so is
libraries are in /usr/lib/i386-linux-gnu/ FOR x86
*/
#include <iostream>
#include <unistd.h>
#include <math.h>
#include "../EngineGlut.h"
#include "Game.h"


#define PI 3.14159265
/*
void drawScene() {
	EngineGlut engine;
	for(int i=0; i<50; i++)
	{
		engine.clearScene();

		engine.beginScene();
		for (int j=0; j<=10000; j++)
		{
			int newCoord = i + j%100;
			//just for tests we have 10.000 surfaces (100 surfaces repeats itself 100 times)
			//just for testing the "capacity" of the engine
			engine.drawSurface(-100+newCoord, 100+newCoord, newCoord, newCoord, -100+newCoord, newCoord);
		}
		engine.endScene();
		usleep(100* 1000);//100 of 1000microseccondes = 100 millisecconds
	}
}
*/
void drawStuff1()
{
	EngineGlut engine;

	Color c = COLOR_1; 

	engine.drawClear();
	for (int i=0; i<10; i++)
	{
	    //int newCoord = i*2;
	    int newCoord = i*10;
	    //just for tests we have 10.000 surfaces (100 surfaces repeats itself 100 times)
	    //just for testing the "capacity" of the engine
	    engine.drawSurface(-100+newCoord, 100+newCoord, newCoord, newCoord, -100+newCoord, newCoord, c);

	    //	engine.drawSurface(-100+newCoord, 100+newCoord, newCoord, newCoord, -100+newCoord, newCoord);
	}


}
void drawStuff()
{
	EngineGlut engine;

	for (int i=0; i<50; i++)
	{
		engine.drawClear();
		for (int j=0; j<=10000; j++)
		{
		//int newCoord = i*2;
			int newCoord = i + j%100;
			//just for tests we have 10.000 surfaces (100 surfaces repeats itself 100 times)
			//just for testing the "capacity" of the engine
			engine.drawSurface(-100+newCoord, 100+newCoord, newCoord, newCoord, -100+newCoord, newCoord);

	//	engine.drawSurface(-100+newCoord, 100+newCoord, newCoord, newCoord, -100+newCoord, newCoord);
		}
		
		usleep(100* 1000);//100 of 1000microseccondes = 100 millisecconds

	}


}

void firstDraw()
{
	int sleepTime = 1;
	EngineGlut engine;
	engine.drawPoint(100, 100);
	sleep(sleepTime);
	engine.drawPoint(102, 102);
	sleep(sleepTime);
	engine.drawLine(-100, -100, 0, 0);
	sleep(sleepTime);
	
	engine.drawSurface(-300, -300, -200, -200, -300, -200);
	sleep(sleepTime);
}

double x_patrat_10(double x)
{
	return x*x / 10;
}

double sinx_100(double x)
{
	return 100*sin(x*PI/180);
}
void tabelareaFunctiilor(double (*funct)(double))
{

	EngineGlut engine;
	engine.drawClear();
	for (int i=-400; i<400; i++)
	{
		engine.drawPoint(i, funct(i));
	}

}

void rotating()
{
	double x = 100; 
	double y = 200;

	double newX = x, newY = y;
	EngineGlut engine;
	engine.drawClear();
	
	int teta = 2;
	double rad = teta*PI/180;
	for (int i=0; i<360; i+= teta)
	{
		x = newX;
		y = newY;
		newX = x * cos(rad) - y * sin(rad);
		newY = x * sin(rad) + y * cos(rad);
		engine.drawClear();
		engine.drawLine(0,0,newX, newY);
		usleep(60* 1000);//100 of 1000microseccondes = 100 millisecconds
	}
}
int main(int argc, char ** argv)
{
	initEngineGlut(argc, argv);

//	drawStuff1();
//	drawScene();
	
//	sleep(sleepTime);

//	tabelareaFunctiilor(&x_patrat_10);
//	sleep(sleepTime);
//	tabelareaFunctiilor(&sinx_100);
//	rotating();

	Game g;
	std::cin.ignore();

}
