#include "Table.h"
#include "../EngineGlut.h"

void drawFrom(int i, int j)
{
    int pas = 70;
    int x = -380;
    int y = 210;

    EngineGlut e;
    e.drawLine(x + j*pas, y - i*pas, x + (j+1)*pas, y- i*pas);
    e.drawLine(x + j*pas, y - (i+1)*pas, x + (j+1)*pas, y- (i+1)*pas);
    e.drawLine(x + j*pas, y - i*pas, x + j*pas, y - (i+1)*pas);
    e.drawLine(x + (j+1)*pas, y - i*pas, x + (j+1)*pas, y - (i+1)*pas);
    
}
Table::Table()
{

    for (int i=0; i<L; i++)
    {
	for (int j=0; j<C; j++)
	    drawFrom(i,j);
    }
    
}
