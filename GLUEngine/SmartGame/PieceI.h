#ifndef PIECE_I
#define PIECE_I
#include "Shape.h"
class PieceI: public Shape
{
    public: 
	PieceI();
	PieceI(std::list<Coord> coord);
    private:
	void initData();
};
#endif

