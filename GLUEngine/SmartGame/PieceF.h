#ifndef PIECE_F
#define PIECE_F
#include "Shape.h"
class PieceF: public Shape
{
    public: 
	PieceF();
	PieceF(std::list<Coord> coord);
    private:
	void initData();
};
#endif
