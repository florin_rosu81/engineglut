#ifndef PIECE_D
#define PIECE_D
#include "Shape.h"
class PieceD: public Shape
{
    public: 
	PieceD();
	PieceD(std::list<Coord> coord);
    private:
	void initData();
};
#endif
