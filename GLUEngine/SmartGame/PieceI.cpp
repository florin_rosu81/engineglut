#include "PieceI.h"

PieceI::PieceI()
{
    initData();

    this->color = COLOR_I;

    variant = allCoord.begin();

}
PieceI::PieceI(std::list<Coord> coord)
{
    this->color = COLOR_I;
    this->coord = coord;
}
void PieceI::initData()
{
   /*THIS  0   0
    *      0 0 0 
    */ 
    std::list<Coord> coordVariant;
    Coord c(0,0);
    coordVariant.push_back(c);
    
    c.line = 1;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = 1;
    c.column = 1;
    coordVariant.push_back(c);

    c.line = 1;
    c.column = 2;
    coordVariant.push_back(c);
    
    c.line = 0;
    c.column = 2;
    coordVariant.push_back(c);

    generateVariants(coordVariant);

   /*      0   0
    * THIS 0 0 0 
    */ 

    coordVariant.clear();
    c.line = 0;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = -1;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = 0;
    c.column = 1;
    coordVariant.push_back(c);

    c.line = 0;
    c.column = 2;
    coordVariant.push_back(c);

    c.line = -1;
    c.column = 2;
    coordVariant.push_back(c);
    
    generateVariants(coordVariant);

   /*      0   0
    *      0 0 0 THIS
    */ 

    coordVariant.clear();
    c.line = 0;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = -1;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = 0;
    c.column = -1;
    coordVariant.push_back(c);

    c.line = 0;
    c.column = -2;
    coordVariant.push_back(c);

    c.line = -1;
    c.column = -2;
    coordVariant.push_back(c);

    generateVariants(coordVariant);
   /*      0   0 THIS
    *      0 0 0 
    */ 

    coordVariant.clear();
    c.line = 0;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = 1;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = 1;
    c.column = -1;
    coordVariant.push_back(c);

    c.line = 1;
    c.column = -2;
    coordVariant.push_back(c);

    c.line = 0;
    c.column = -2;
    coordVariant.push_back(c);
    
    generateVariants(coordVariant);
}

