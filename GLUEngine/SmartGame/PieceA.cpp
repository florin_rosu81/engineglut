#include "PieceA.h"

PieceA::PieceA()
{
    initData();

    this->color = COLOR_A;

    variant = allCoord.begin();

}
PieceA::PieceA(std::list<Coord> coord)
{
    this->color = COLOR_A;
    this->coord = coord;
}
void PieceA::initData()
{
   /*      0 0 0
    * THIS 0 
    */ 
    std::list<Coord> coordVariant;
    Coord c(0,0);
    coordVariant.push_back(c);
    
    c.line = -1;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = -1;
    c.column = 1;
    coordVariant.push_back(c);

    c.line = -1;
    c.column = 2;
    coordVariant.push_back(c);

    generateVariants(coordVariant);


   /* THIS 0 0 0
    *      0 
    */ 
    coordVariant.clear();
    c.line = 0;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = 1;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = 0;
    c.column = 1;
    coordVariant.push_back(c);

    c.line = 0;
    c.column = 2;
    coordVariant.push_back(c);

    generateVariants(coordVariant);

   /* THIS 0 0 0
    *          0 
    */ 
    coordVariant.clear();
    c.line = 0;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = 1;
    c.column = 2;
    coordVariant.push_back(c);
    
    c.line = 0;
    c.column = 1;
    coordVariant.push_back(c);

    c.line = 0;
    c.column = 2;
    coordVariant.push_back(c);

    generateVariants(coordVariant);
}
