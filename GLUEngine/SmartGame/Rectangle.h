#ifndef RECTANGLE_H 
#define RECTANGLE_H

#include "DrawingShape.h"
#include "Point.h"

class Rectangle: public DrawingShape
{
public:
    Rectangle(int line, int colomn, Color color, bool full=false);
	
	virtual void draw();

private:
	void drawFull();
	void drawSimple();


	Point p1, p2, p3, p4;
	bool full;
	Color c;
};

#endif
