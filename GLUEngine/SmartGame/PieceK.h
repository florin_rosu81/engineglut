#ifndef PIECE_K
#define PIECE_K
#include "Shape.h"
class PieceK: public Shape
{
    public: 
	PieceK();
	PieceK(std::list<Coord> coord);
    private:
	void initData();
};
#endif
