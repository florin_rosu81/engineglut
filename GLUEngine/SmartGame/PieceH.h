#ifndef PIECE_H
#define PIECE_H
#include "Shape.h"
class PieceH: public Shape
{
    public: 
	PieceH();
	PieceH(std::list<Coord> coord);
    private:
	void initData();
};
#endif

