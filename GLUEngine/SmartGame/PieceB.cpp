
#include "PieceB.h"

PieceB::PieceB()
{
    initData();

    this->color = COLOR_B;

    variant = allCoord.begin();

}
PieceB::PieceB(std::list<Coord> coord)
{
    this->color = COLOR_B;
    this->coord = coord;
}
void PieceB::initData()
{
   /*        0 
    * THIS 0 0
    *      0 0 
    */ 
    std::list<Coord> coordVariant;
    Coord c(0,0);
    coordVariant.push_back(c);
    
    c.line = 1;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = 1;
    c.column = 1;
    coordVariant.push_back(c);

    c.line = 0;
    c.column = 1;
    coordVariant.push_back(c);
    
    c.line = -1;
    c.column = 1;
    coordVariant.push_back(c);

    generateVariants(coordVariant);
   
    
   /*        0 
    *      0 0
    * THIS 0 0 
    */ 
    coordVariant.clear();
    c.line = c.column = 0;
    coordVariant.push_back(c);
    
    c.line = 0;
    c.column = 1;
    coordVariant.push_back(c);
    
    c.line = -1;
    c.column = 0;
    coordVariant.push_back(c);

    c.line = -1;
    c.column = 1;
    coordVariant.push_back(c);
    
    c.line = -2;
    c.column = 1;
    coordVariant.push_back(c);

    generateVariants(coordVariant);
  
   
   /*        0 
    *      0 0
    *      0 0 THIS 
    */ 
    coordVariant.clear();
    c.line = c.column = 0;
    coordVariant.push_back(c);
    
    c.line = 0;
    c.column = -1;
    coordVariant.push_back(c);
    
    c.line = -1;
    c.column = 0;
    coordVariant.push_back(c);

    c.line = -1;
    c.column = -1;
    coordVariant.push_back(c);
    
    c.line = -2;
    c.column = 0;
    coordVariant.push_back(c);

    generateVariants(coordVariant);

   /* THIS   0 
    *      0 0
    *      0 0  
    */ 
    coordVariant.clear();
    c.line = c.column = 0;
    coordVariant.push_back(c);
    
    c.line = 1;
    c.column = -1;
    coordVariant.push_back(c);
    
    c.line = 1;
    c.column = 0;
    coordVariant.push_back(c);

    c.line = 2;
    c.column = -1;
    coordVariant.push_back(c);
    
    c.line = 2;
    c.column = 0;
    coordVariant.push_back(c);

    generateVariants(coordVariant);
}
