#ifndef DRAWING_SHAPE_H
#define DRAWING_SHAPE_H

#include "EngineGlut.h"

class DrawingShape
{
public:
	virtual void draw() = 0;
protected:
	static EngineGlut engine;
};


#endif
