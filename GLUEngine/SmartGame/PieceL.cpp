#include "PieceL.h"

PieceL::PieceL()
{
    initData();

    this->color = COLOR_L;

    variant = allCoord.begin();

}
PieceL::PieceL(std::list<Coord> coord)
{
    this->color = COLOR_L;
    this->coord = coord;
}
void PieceL::initData()
{
   /*        0
    * THIS 0 0 0
    *        0
    */ 
    std::list<Coord> coordVariant;
    Coord c(0,0);
    coordVariant.push_back(c);
    
    c.line = -1;
    c.column = 1;
    coordVariant.push_back(c);
    
    c.line = 0;
    c.column = 1;
    coordVariant.push_back(c);

    c.line = 1;
    c.column = 1;
    coordVariant.push_back(c);

    c.line = 0;
    c.column = 2;
    coordVariant.push_back(c);
    
    generateVariants(coordVariant);

}

