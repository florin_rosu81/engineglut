#ifndef PIECE_A
#define PIECE_A
#include "Shape.h"
class PieceA: public Shape
{
    public: 
	PieceA();
	PieceA(std::list<Coord> coord);
    private:
	void initData();
};
#endif
