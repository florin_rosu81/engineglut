#include <iostream>
#include "Shape.h"
#include "Rectangle.h"

Shape::Shape()
{
}

Shape::~Shape()
{
}

void Shape::draw()
{
    std::list<Coord>::iterator it;
    for (it=this->coord.begin(); it != this->coord.end(); it++)
    {
	Rectangle r(it->line, it->column, color, true);
	r.draw();
    }

}

std::list<Coord> Shape::getCoord()
{
    return this->coord;
}

bool Shape::isValid()
{
    return valid;
}

void Shape::setCoord(int line, int column)
{
    this->line = line;
    this->column = column;

    variant = allCoord.begin();

    updateCoord();
}

bool Shape::nextVariant()
{
    variant++;
    if (variant == allCoord.end())
	return false;
    updateCoord();
    return true;
}

std::list<Coord> Shape::mirror(std::list<Coord> l)
{
    std::list<Coord> res;
    std::list<Coord>::iterator it;
    for (it = l.begin(); it != l.end(); it++)
    {
	Coord c;
	c.line = - it->line;
	c.column = it->column;

	res.push_back(c);
    }

    return res;

}
std::list<Coord> Shape::rotate(std::list<Coord> l)
{
    std::list<Coord> res;
    std::list<Coord>::iterator it;
    for (it = l.begin(); it != l.end(); it++)
    {
	Coord c;
	c.line =  it->column;
	c.column = it->line;

	res.push_back(c);
    }

    return res;
}
std::list<Coord> Shape::flip(std::list<Coord> l)
{
    std::list<Coord> res;
    std::list<Coord>::iterator it;
    for (it = l.begin(); it != l.end(); it++)
    {
	Coord c;
	c.line =  -it->line;
	c.column = - it->column;

	res.push_back(c);
    }

    return res;
}

void Shape::generateVariants(std::list<Coord> coordVariant)
{
    //std::cout<<"VARIANT"<<std::endl;
    std::list<Coord> varMirror = mirror(coordVariant);

    std::list<Coord> varRotate = rotate(coordVariant);
    std::list<Coord> varRotateMirror = rotate(varMirror);
/*
    allCoord.push_back(coordVariant);
    allCoord.push_back(varMirror);
    allCoord.push_back(varRotate);
    allCoord.push_back(varRotateMirror);

    allCoord.push_back(flip(coordVariant));
    allCoord.push_back(flip(varMirror));
    allCoord.push_back(flip(varRotate));
    allCoord.push_back(flip(varRotateMirror));
*/
    insertVariant(coordVariant);
    insertVariant(varMirror);
    insertVariant(varRotate);
    insertVariant(varRotateMirror);

    insertVariant(flip(coordVariant));
    insertVariant(flip(varMirror));
    insertVariant(flip(varRotate));
    insertVariant(flip(varRotateMirror));

}

void Shape::insertVariant(std::list<Coord> coordVariant)
{
    std::list<Coord>::iterator i;
    for (i=coordVariant.begin(); i != coordVariant.end(); i++)
    {
	//std::cout<<i->line<<" "<<i->column<<";";
    }
    bool toInsert = true;
    std::list<std::list<Coord> >::iterator it;
    for (it = allCoord.begin(); it != allCoord.end(); it++)
    {
	std::list<Coord>::iterator it1, it2;
	bool equals=true;
	for (it1 = it->begin(); it1 != it->end(); it1++)
	{
	    bool found = false;
	    for (it2 = coordVariant.begin(); it2 != coordVariant.end(); it2++)
		if ((*it1)==(*it2))
		    found = true;
	    if (found == false)
		equals = false;
	}
	if (equals == true)
	    toInsert = false;

    }
    if (toInsert)
	allCoord.push_back(coordVariant);
    /*
    else
	std::cout<<"DUPLICATE"<<std::endl;
    
    std::cout<<std::endl;
    */
}
void Shape::updateCoord()
{
    valid = true;
    this->coord.clear();
    std::list<Coord>::iterator it;
    for (it=variant->begin(); it!=variant->end(); it++)
    {
	Coord c(line+it->line, column + it->column);
	if ((c.line < 0) || (c.line >= LINE) || (c.column < 0) || (c.column >= COLUMN))
	    valid = false;
	coord.push_back(c);
    }
}
