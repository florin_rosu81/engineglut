#include "Coord.h"
Coord::Coord()
{
    line = column = 0;
}
Coord::Coord(int line, int column):
    line(line),
    column(column)
{
}

bool Coord::operator==(const Coord o) const
{
    return ((line==o.line)&&(column==o.column));
}
