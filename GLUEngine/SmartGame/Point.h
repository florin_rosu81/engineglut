#ifndef POINT_H
#define POINT_H

#include "DrawingShape.h"

class Point : public DrawingShape
{
    public:
        Point();
        Point(double x, double y);
        ~Point();

	double getX();
	double getY();

        virtual void draw();
    protected:
    private:
	double X,Y;
};

#endif // POINT_H
