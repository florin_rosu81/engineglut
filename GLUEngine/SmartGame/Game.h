#ifndef GAME_H
#define GAME_H

#include <list>
#include "Table.h"
#include "Shape.h"

#define N 12

class Game
{
    public: 
	Game(int argc, char** argv);
	~Game();
	void start();

    private:

	void redraw();
	void readData();
	Shape* createPiece(char pieceChar, std::list<Coord> coord);
	Shape* createPiece(char pieceChar);

	Table *t;
	bool piecesAlready[N];
	Shape *pieces[N];
	int nrPieces;
	int table[LINE][COLUMN];
	int sol[N];
	int solN;

	bool solution();
	Coord getNextPosition();
	bool checkNextVariant(int k);
	bool nextVariant(int k);
	void updateTable(int k);
};



#endif
