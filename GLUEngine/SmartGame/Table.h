#ifndef TABLE_H
#define TABLE_H
#include <list>
#include "Shape.h"


class Table
{
    public:
	Table(std::list<Shape*> pieces);
	~Table();
	void draw();

    private:
	std::list<Shape*> pieces;
};

#endif
