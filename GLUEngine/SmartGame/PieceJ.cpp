#include "PieceJ.h"

PieceJ::PieceJ()
{
    initData();

    this->color = COLOR_J;

    variant = allCoord.begin();

}
PieceJ::PieceJ(std::list<Coord> coord)
{
    this->color = COLOR_J;
    this->coord = coord;
}
void PieceJ::initData()
{
   /* THIS 0 0 0 0
    */ 
    std::list<Coord> coordVariant;
    Coord c(0,0);
    coordVariant.push_back(c);
    
    c.line = 0; 
    c.column = 1;
    coordVariant.push_back(c);

    c.line = 0;
    c.column = 2;
    coordVariant.push_back(c);

    c.line = 0;
    c.column = 3;
    coordVariant.push_back(c);
    
    generateVariants(coordVariant);

}

