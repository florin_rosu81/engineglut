#ifndef COORD_H
#define COORD_H

#define LINE 5
#define COLUMN 11

struct Coord
{
	Coord();
	Coord(int line, int column);
	int line, column;
	bool operator==(const Coord) const;
};
#endif
