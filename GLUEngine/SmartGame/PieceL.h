#ifndef PIECE_L
#define PIECE_L
#include "Shape.h"
class PieceL: public Shape
{
    public: 
	PieceL();
	PieceL(std::list<Coord> coord);
    private:
	void initData();
};
#endif

