#ifndef PIECE_B
#define PIECE_B
#include "Shape.h"
class PieceB: public Shape
{
    public: 
	PieceB();
	PieceB(std::list<Coord> coord);
    private:
	void initData();
};
#endif
