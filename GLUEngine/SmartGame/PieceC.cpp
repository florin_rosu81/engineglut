
#include "PieceC.h"

PieceC::PieceC()
{
    initData();

    this->color = COLOR_C;

    variant = allCoord.begin();

}
PieceC::PieceC(std::list<Coord> coord)
{
    this->color = COLOR_C;
    this->coord = coord;
}
void PieceC::initData()
{
   /*      0 0 0 0
    * THIS 0 
    */ 
    std::list<Coord> coordVariant;
    Coord c(0,0);
    coordVariant.push_back(c);
    
    c.line = -1;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = -1;
    c.column = 1;
    coordVariant.push_back(c);

    c.line = -1;
    c.column = 2;
    coordVariant.push_back(c);

    c.line = -1;
    c.column = 3;
    coordVariant.push_back(c);
    
    generateVariants(coordVariant);


   /* THIS 0 0 0 0
    *      0 
    */ 
    coordVariant.clear();
    c.line = 0;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = 1;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = 0;
    c.column = 1;
    coordVariant.push_back(c);

    c.line = 0;
    c.column = 2;
    coordVariant.push_back(c);
    
    c.line = 0;
    c.column = 3;
    coordVariant.push_back(c);

    generateVariants(coordVariant);

   /* THIS 0 0 0 0
    *            0 
    */ 
    coordVariant.clear();
    c.line = 0;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = 1;
    c.column = 3;
    coordVariant.push_back(c);
    
    c.line = 0;
    c.column = 1;
    coordVariant.push_back(c);

    c.line = 0;
    c.column = 2;
    coordVariant.push_back(c);
    
    c.line = 0;
    c.column = 3;
    coordVariant.push_back(c);

    generateVariants(coordVariant);
}
