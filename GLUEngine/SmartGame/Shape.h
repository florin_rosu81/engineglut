#ifndef SHAPE_H
#define SHAPE_H
#include <list>
#include "Coord.h"
#include "EngineGlut.h"

class Shape
{
    public:
	Shape();
	virtual ~Shape();
	void draw();
	std::list<Coord> getCoord();
	bool isValid();
	void setCoord(int line, int column);
	bool nextVariant();

protected:

	void generateVariants(std::list<Coord> coordVariant);

	int line, column;
	
	std::list<Coord> coord;
	std::list<std::list<Coord> > allCoord;
	std::list<std::list<Coord> >::iterator variant;
	Color color;
	bool valid;
private:
	void updateCoord();
	std::list<Coord> mirror(std::list<Coord>);
	std::list<Coord> rotate(std::list<Coord>);
	std::list<Coord> flip(std::list<Coord>);

	void insertVariant(std::list<Coord>);


};

#endif
