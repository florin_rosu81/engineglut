#include <unistd.h>
#include "Table.h"
#include "EngineGlut.h"
#include "Rectangle.h"

#include "PieceA.h"
Table::Table(std::list<Shape*> pieces):
    pieces(pieces)
{
}
Table::~Table()
{
    std::list<Shape*>::iterator it;
    for(it=pieces.begin(); it!= pieces.end(); it++)
	delete *it;
    pieces.clear();
}
void Table::draw()
{
    for (int i=0; i<LINE; i++)
    {
	for (int j=0; j<COLUMN; j++)
	{
	    Rectangle r1(i,j,COLOR_A, false);
	    r1.draw();
	}
    }
    std::list<Shape*>::iterator it;
    for (it = pieces.begin(); it != pieces.end(); it++)
    {
	(*it)->draw();
    } 
}
