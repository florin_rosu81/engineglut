/**
 * export LD_LIBRARY_PATH where libengineGlut.so is
libraries are in /usr/lib/i386-linux-gnu/ FOR x86
*/
#include <iostream>
#include "EngineGlut.h"
#include "Game.h"


int main(int argc, char ** argv)
{
	Game g(argc, argv);
	g.start();
	std::cin.ignore();
}
