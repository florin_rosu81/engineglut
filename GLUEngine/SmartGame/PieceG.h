#ifndef PIECE_G
#define PIECE_G
#include "Shape.h"
class PieceG: public Shape
{
    public: 
	PieceG();
	PieceG(std::list<Coord> coord);
    private:
	void initData();
};
#endif

