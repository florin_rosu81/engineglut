
#ifndef PIECE_C
#define PIECE_C
#include "Shape.h"
class PieceC: public Shape
{
    public: 
	PieceC();
	PieceC(std::list<Coord> coord);
    private:
	void initData();
};
#endif
