#include "PieceF.h"

PieceF::PieceF()
{
    initData();

    this->color = COLOR_F;

    variant = allCoord.begin();

}
PieceF::PieceF(std::list<Coord> coord)
{
    this->color = COLOR_F;
    this->coord = coord;
}
void PieceF::initData()
{
   /*      0 0 
    * THIS 0 
    */ 
    std::list<Coord> coordVariant;
    Coord c(0,0);
    coordVariant.push_back(c);
    
    c.line = -1;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = -1;
    c.column = 1;
    coordVariant.push_back(c);

    generateVariants(coordVariant);


   /* THIS 0 0 
    *      0 
    */ 
    coordVariant.clear();
    c.line = 0;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = 1;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = 0;
    c.column = 1;
    coordVariant.push_back(c);

    generateVariants(coordVariant);

   /* THIS   0 0
    *          0 
    */ 
    coordVariant.clear();
    c.line = 0;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = 1;
    c.column = 1;
    coordVariant.push_back(c);
    
    c.line = 0;
    c.column = 1;
    coordVariant.push_back(c);

    generateVariants(coordVariant);
}
