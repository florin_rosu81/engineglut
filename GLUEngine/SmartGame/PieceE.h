#ifndef PIECE_E
#define PIECE_E
#include "Shape.h"
class PieceE: public Shape
{
    public: 
	PieceE();
	PieceE(std::list<Coord> coord);
    private:
	void initData();
};
#endif
