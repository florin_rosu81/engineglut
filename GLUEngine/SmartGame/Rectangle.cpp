#include "Rectangle.h"

int pas = 70;
int x = -380;
int y = 210;

Rectangle::Rectangle(int line, int colomn, Color color, bool full):
    p1(x+colomn*pas + full, y-line*pas-full),
    p2(x+(colomn+1)*pas-full, y-line*pas-full),
    p3(x+(colomn+1)*pas-full, y-(line+1)*pas+full),
    p4(x+colomn*pas+full, y-(line+1)*pas+full),
    c(color),
    full(full)
{
}

void Rectangle::draw()
{
	if (this->full)
		this->drawFull();
	else
		this->drawSimple();
}

void Rectangle::drawFull()
{
	engine.drawSurface(p1.getX(), p1.getY(), p2.getX(), p2.getY(), p3.getX(), p3.getY(), c);
	engine.drawSurface(p1.getX(), p1.getY(), p4.getX(), p4.getY(), p3.getX(), p3.getY(), c);
}
void Rectangle::drawSimple()
{
    engine.drawLine(p1.getX(), p1.getY(), p2.getX(),p2.getY());
    engine.drawLine(p2.getX(), p2.getY(), p3.getX(),p3.getY());
    engine.drawLine(p3.getX(), p3.getY(), p4.getX(),p4.getY());
    engine.drawLine(p1.getX(), p1.getY(), p4.getX(),p4.getY());
}
