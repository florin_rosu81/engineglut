#include <unistd.h>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include "Game.h"
#include "EngineGlut.h"
#include "PieceA.h"
#include "PieceB.h"
#include "PieceC.h"
#include "PieceD.h"
#include "PieceE.h"
#include "PieceF.h"
#include "PieceG.h"
#include "PieceH.h"
#include "PieceI.h"
#include "PieceJ.h"
#include "PieceK.h"
#include "PieceL.h"

Game::Game(int argc, char** argv)
{
	initEngineGlut(argc, argv);
	for (int i=0; i<12; i++)
	    piecesAlready[i] = false;
	nrPieces = 0;

	for (int i=0; i<LINE; i++)
	    for (int j=0; j<COLUMN; j++)
		table[i][j] = 0;
}
Game::~Game()
{
    delete t;
    for(int i=0; i<nrPieces; i++)
	delete pieces[i];
}

void Game::readData()
{
    std::string line;
    std::ifstream f("inputPuzzle.txt");
    
    std::list<Shape*>puzzlePieces;
    if (f.is_open())
    {
	while (getline(f, line))
	{
	    std::stringstream s(line);
	    char pieceChar;
	    s >> pieceChar;

	    std::list<Coord> coord;
	    while(s)
	    {
		Coord c;
		s >> c.line >> c.column;
		if (!s)
		    break;
		coord.push_back(c);
		table[c.line][c.column] = 1;
	    }
	    puzzlePieces.push_back(createPiece(pieceChar, coord));
	    
	}

	t = new Table(puzzlePieces);
	nrPieces = 0;
	for (char c='A'; c<='L'; c++)
	{
	    if (piecesAlready[c-'A'] == false)
	    {
		std::cout<<"Piece "<<nrPieces<<":"<<c<<std::endl;
		pieces[nrPieces] = createPiece(c);
		nrPieces++;
	    }
	}
    }
}

Shape* Game::createPiece(char pieceChar)
{
    switch(pieceChar)
    {
	case 'A': return new PieceA();
	case 'B': return new PieceB();
	case 'C': return new PieceC();
	case 'D': return new PieceD();
	case 'E': return new PieceE();
	case 'F': return new PieceF();
	case 'G': return new PieceG();
	case 'H': return new PieceH();
	case 'I': return new PieceI();
	case 'J': return new PieceJ();
	case 'K': return new PieceK();
	case 'L': return new PieceL();
    };
    return NULL;
}
Shape* Game::createPiece(char pieceChar, std::list<Coord> coord)
{
    piecesAlready[pieceChar - 'A'] = true;
    switch(pieceChar)
    {
	case 'A': return new PieceA(coord);
	case 'B': return new PieceB(coord);
	case 'C': return new PieceC(coord);
	case 'D': return new PieceD(coord);
	case 'E': return new PieceE(coord);
	case 'F': return new PieceF(coord);
	case 'G': return new PieceG(coord);
	case 'H': return new PieceH(coord);
	case 'I': return new PieceI(coord);
	case 'J': return new PieceJ(coord);
	case 'K': return new PieceK(coord);
	case 'L': return new PieceL(coord);
    };
    return NULL;
}

bool Game::solution()
{
    for(int i=0; i<LINE; i++)
	for(int j=0; j<COLUMN; j++)
	    if (table[i][j]==0)
		return false;
    return true;
}
Coord Game::getNextPosition()
{
    for (int j=0; j<COLUMN; j++)
	for(int i=0; i<LINE; i++)
	    if (table[i][j] == 0)
	    {
		    Coord c(i,j);
		    return c;
	    }
}

bool Game::checkNextVariant(int k)
{

    bool isValid = true;
    
    std::list<Coord> coordonates = pieces[sol[k]]->getCoord();
    std::list<Coord>::iterator it;
    for (it = coordonates.begin(); it!= coordonates.end(); it++)
    {
	if (table[it->line][it->column] != 0)
	    isValid = false;
    }

    return isValid;
}

bool Game::nextVariant(int k)
{
    if (sol[k]==-1)
    {
	
	for(sol[k]=0; sol[k]<nrPieces; sol[k]++)
	{
	    bool nextOk = true;
	    for (int j=0; j<k; j++)
		if (sol[k]==sol[j])
		    nextOk = false;
	    if (nextOk)
		break;
	}
	
	Coord c = getNextPosition();
	pieces[sol[k]]->setCoord(c.line, c.column);
    }
    else
    {
	bool check = pieces[sol[k]]->nextVariant();
	if (check==false)
	{
	    for(sol[k]+=1; sol[k]<nrPieces; sol[k]++)
	    {
		bool nextOk = true;
		for (int j=0; j<k; j++)
		    if (sol[k]==sol[j])
			nextOk = false;
		if (nextOk)
		    break;
	    }   

	    if (sol[k]<nrPieces)
	    {
		Coord c = getNextPosition();
		pieces[sol[k]]->setCoord(c.line, c.column);
	    }
	    else return false;
	}
    }
    return true;
}

void Game::updateTable(int k)
{
    
    for (int i=0; i<LINE; i++)
	for (int j=0; j<COLUMN; j++)
	    if (table[i][j] == 2)
		table[i][j] = 0;
    
    for (int i=0; i<k; i++)
    {
	std::list<Coord>::iterator it;
	std::list<Coord> list = pieces[sol[i]]->getCoord();
	for (it=list.begin(); it!=list.end(); it++)
	{
	    table[it->line][it->column] = 2;
	}
    }
   
    
}
void Game::start()
{
	readData();
	solN = -1;
	redraw();
	usleep(2000 * 1000);
	solN = 0;
	sol[0] = -1;
	
	bool haveSolution = false;
	do
	{
	    while ((!haveSolution) && nextVariant(solN))
	    {
		bool valid = true;
		if(valid = pieces[sol[solN]]->isValid())
		{
		    valid = checkNextVariant(solN);
		}

		if (valid)
		{

		    updateTable(solN+1);
		    redraw();
		    usleep(1000 * 1000);
		    
		    if (solution())
		    {
			//usleep(10000 * 1000);
			haveSolution = true;
		    }
		    else
		    {
			solN++;
			sol[solN]=-1;
		    }
		}
	    }
	    solN--;
	    updateTable(solN);
	}while((!haveSolution) && (solN>=0));	
	std::cout<<"END GAME "<<haveSolution;
}
void Game::redraw()
{
    EngineGlut e;
    e.drawClear();
    t->draw();
    for (int i=0; i<=solN; i++)
	pieces[sol[i]]->draw();
}
