#ifndef PIECE_J
#define PIECE_J
#include "Shape.h"
class PieceJ: public Shape
{
    public: 
	PieceJ();
	PieceJ(std::list<Coord> coord);
    private:
	void initData();
};
#endif

