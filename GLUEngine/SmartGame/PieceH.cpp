#include "PieceH.h"

PieceH::PieceH()
{
    initData();

    this->color = COLOR_H;

    variant = allCoord.begin();

}
PieceH::PieceH(std::list<Coord> coord)
{
    this->color = COLOR_H;
    this->coord = coord;
}
void PieceH::initData()
{
   /*          0 
    *        0 0
    * THIS 0 0
    */ 
    std::list<Coord> coordVariant;
    Coord c(0,0);
    coordVariant.push_back(c);
    
    c.line = 0;
    c.column = 1;
    coordVariant.push_back(c);

    c.line = -1;
    c.column = 1;
    coordVariant.push_back(c);
    
    c.line = -1;
    c.column = 2;
    coordVariant.push_back(c);
    
    c.line = -2;
    c.column = 2;
    coordVariant.push_back(c);

    generateVariants(coordVariant);

   /*          0 
    *        0 0
    *      0 0 THIS
    */ 

    coordVariant.clear();
    c.line = 0;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = 0;
    c.column = -1;
    coordVariant.push_back(c);
    
    c.line = -1;
    c.column = 0;
    coordVariant.push_back(c);

    c.line = -1;
    c.column = 1;
    coordVariant.push_back(c);
    
    c.line = -2;
    c.column = 1;
    coordVariant.push_back(c);

    generateVariants(coordVariant);

   /*          0 
    * THIS   0 0
    *      0 0 
    */ 

    coordVariant.clear();
    c.line = 0;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = 1;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = 1;
    c.column = -1;
    coordVariant.push_back(c);

    c.line = 0;
    c.column = 1;
    coordVariant.push_back(c);

    c.line = -1;
    c.column = 1;
    coordVariant.push_back(c);
    
    generateVariants(coordVariant);
    
   /*          0 
    *        0 0 THIS
    *      0 0 
    */ 

    coordVariant.clear();
    c.line = 0;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = -1;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = 0;
    c.column = -1;
    coordVariant.push_back(c);

    c.line = 1;
    c.column = -1;
    coordVariant.push_back(c);

    c.line = 1;
    c.column = -2;
    coordVariant.push_back(c);
    
    generateVariants(coordVariant);
    
   /*          0 THIS 
    *        0 0 
    *      0 0 
    */ 

    coordVariant.clear();
    c.line = 0;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = 1;
    c.column = 0;
    coordVariant.push_back(c);
    
    c.line = 1;
    c.column = -1;
    coordVariant.push_back(c);

    c.line = 2;
    c.column = -1;
    coordVariant.push_back(c);

    c.line = 2;
    c.column = -2;
    coordVariant.push_back(c);

    generateVariants(coordVariant);
}

