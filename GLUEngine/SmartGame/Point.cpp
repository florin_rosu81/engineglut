#include <iostream>
#include "Point.h"

#include "EngineGlut.h"


Point::Point()
{
    X=Y=0;
}

Point::Point(double x, double y)
{
	X = x;
	Y = y;

}

Point::~Point()
{
    //dtor
}

double Point::getX()
{
	return X;
}
double Point::getY()
{
	return Y;
}

void Point::draw()
{
	//engine from Shape
    this->engine.drawPoint(X,Y);
}
