#ifndef ENGINE_GLUT
#define ENGINE_GLUT

//CLASS NOT USED

void initEngineGlut(int argc, char **argv);

class EngineGlut
{
public:
	EngineGlut();

	void drawPoint(int x, int y);
	void drawLine(int x1, int y1, int x2, int y2);
	void drawSurface(int x1, int y1, int x2, int y2, int x3, int y3);



	//for animations use the scene - LATER USE
	void clearScene();
	void beginScene();
	//call drawPoint, drawLine and drawSurface between begin/end calls
	void endScene();

private:

	bool drawScene;
};

#endif
