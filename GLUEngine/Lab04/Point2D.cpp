#include <iostream>
#include <math.h>
#include "Point2D.h"

#include "EngineGlut.h"
int Point2D::count = 0;

Point2D::Point2D()
{
    this->x = this->y = 0;
    count++;
}

Point2D::Point2D(double x, double y)
{
    this->x = x;
    this->y = y;

    count++;
}

Point2D::~Point2D()
{
	count--;
}

int Point2D::getCount()
{
	return count;
}
void Point2D::print()
{
    std::cout<<"Point at coordinates " <<this->x<<" and "<<this->y<<std::endl;
}

void Point2D::draw()
{
    EngineGlut engine;
    engine.drawPoint(this->x, this->y);
}

double distanceFrom(Point2D &a, Point2D &b)
{
	return sqrt  ((a.x-b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y));
}
