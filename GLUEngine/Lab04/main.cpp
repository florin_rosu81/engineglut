#include <iostream>
#include "Point2D.h"
#include "EngineGlut.h"
#include <math.h>

using namespace std;

#define PI 3.1415

double function_sin(double x)
{
	double rad = x*PI/180;

	return 100*sin(rad);
}

double function_square(double x)
{
	return x*x/100;
}
void drawFunction()
{
    double (*function)(double);

    function = &function_sin;
    for (double x=-400; x<400; x+=1)
    {
	    Point2D point(x,function(x));  
	    point.draw();
    }
}


void testPoint()
{
    Point2D pointA;
    pointA.print();

    cin.ignore();

    Point2D pointB(2,3);
    pointB.print();

    cout<<"Total points get info from A= "<<pointA.getCount()<<endl;
    cout<<"Total points get info from B= "<<pointB.getCount()<<endl;

    cout<<"Distance = "<< distanceFrom(pointA, pointB)<<endl;

    {
	    Point2D pointC(2,5);
	    pointC.print();
    
	    cout<<"Total points with pointC= "<<pointA.getCount()<<endl;
	    cout<<"Distance = "<< distanceFrom(pointB, pointC)<<endl;
	    cin.ignore();

    }

    cout<<"Total points without pointC= "<<pointA.getCount()<<endl;
    cin.ignore();
}

int main(int argc, char** argv)
{
    testPoint();

    initEngineGlut(argc, argv);
    drawFunction();
    cin.ignore();

    return 0;
}
