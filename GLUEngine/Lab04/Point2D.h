#ifndef POINT2D_H
#define POINT2D_H


class Point2D
{
    public:
        Point2D();
        Point2D(double x, double y);
        ~Point2D();
	
	int getCount();

	friend double distanceFrom(Point2D& a, Point2D &b);
        void print();
        void draw();
    protected:
    private:
        double x;
        double y;

	static int count;
};

#endif // POINT2D_H
